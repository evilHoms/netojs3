'use strict';

var positions = [
        'Отвертка ультрозвуковая WHO-D',
        'Ховерборд Mattel 2016',
        'Нейтрализатор FLASH black edition',
        'Меч световой FORCE (синий луч)',
        'Машина времени DeLorean',
        'Репликатор домашний STAR-94',
        'Лингвенсор 000-17',
        'Целеуказатель электронный WAY-Y'
      ]

var posLength = positions.length;

console.log(`Список наименований`);

for (var i = 0; i < positions.length; i++) {
  console.log(`${i + 1} '${positions[i]}'`);
}

positions.push(`Экзоскелет Trooper-111`, `Нейроинтерфейс игровой SEGUN`, `Семена дерева Эйва`);

console.log(`\nОкончательный список наименований`);

for (i = 0; i < positions.length; i++) {
  console.log(`${i + 1} '${positions[i]}'`);
}

var topIndex = positions.indexOf(`Машина времени DeLorean`);
var topItem = positions.splice(topIndex, 1).join();

positions.unshift(topItem);

console.log(`\nПринять в первую очередь`);

for (i = 0; i < 3; i++) {
  console.log(`${i + 1} '${positions[i]}'`);
}


var [topPos, ...others]= [positions.splice(0, 5), positions.join(', ')];

console.log(`\nВ магазине`);

for (i = 0; i < topPos.length; i++) {
  console.log(`${i + 1} '${topPos[i]}'`);
}

console.log(`\nОстальные товары`);

console.log(others[0]);